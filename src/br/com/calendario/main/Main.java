package br.com.calendario.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		
	    try {
	    	int ano = Integer.parseInt(JOptionPane.showInputDialog("Informe o ano que deseja calcular as datas da 3° semana "));
	    	String resultado = "";
	    	
	    	
	    	for (int i = 0; i < 12; i++) {
	    		Calendar calendar = GregorianCalendar.getInstance();
	    		calendar.set(ano, i, 1);
	    		int qtdDiasDoMes = calendar.getActualMaximum (Calendar.DAY_OF_MONTH);
	    		for (int k = 1; k < qtdDiasDoMes; k++) {
	    			Calendar cal = GregorianCalendar.getInstance();
	    			cal.set(ano, i, k);
	    			
	    			int semana = cal.get(Calendar.DAY_OF_WEEK_IN_MONTH);
	    			int diaDaSemana = cal.get(Calendar.DAY_OF_WEEK);
	    			
	    			if((semana == 3 && diaDaSemana == 1) || (semana == 3 && diaDaSemana == 7)){
	    				String week = diaDaSemana == 1 ? " Domingo" : " Sabado" ;
	    				resultado += new SimpleDateFormat("dd/MM/yyy").format(cal.getTime()) + 
	    						" " + week+"\n";
	    				
	    			}
	    		}
	    		resultado +="-----------------------------"+"\n";
	    		
	    	}
	    	JOptionPane.showMessageDialog(null,resultado);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Ano informado esta incorreto, digite um valor válido");
		}
		
	}

}
	